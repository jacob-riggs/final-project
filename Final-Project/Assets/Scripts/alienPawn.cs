﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alienPawn : pawn {

    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Transform tf;
    private Animator anim;
    private Animator enemyAnim;

    public float moveSpeed;
    public float jumpForce;
    public bool isGrounded;
    public float groundDistance = 0.1f;

    public int maxAirJumps;
    public int currentAirJumps;

    public AudioClip jumpSound;

    public void Start() {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        tf = GetComponent<Transform>();
        anim = GetComponent<Animator>();
    }

    public void Update() {
        //Check if player is grounded
        CheckForGrounded();
    }

    public void CheckForGrounded() {
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector3.down, groundDistance);
        if (hitInfo.collider == null) {
            isGrounded = false;
            anim.Play("alienJump");
        }
        else if (hitInfo.collider.tag == "Enemy") {
            Destroy(hitInfo.collider.gameObject);
        }
        else {
            isGrounded = true;
            currentAirJumps = maxAirJumps; // Resets the currentAirJumps to 1 after the player lands
        }
    }

    public override void Move(float xMovement) {
        rb.velocity = new Vector2(xMovement * moveSpeed, rb.velocity.y);

        // If velocity is <0, flip sprite
        if (isGrounded && rb.velocity.x < 0) {
            anim.Play("alienWalk");
            sr.flipX = true;
        }
        else if (isGrounded && rb.velocity.x > 0) {
            anim.Play("alienWalk");
            sr.flipX = false;
        }
        else if (isGrounded) {
            anim.Play("alienIdle");
        }
        else if (rb.velocity.x < 0) {
            sr.flipX = true;
        }
        else if (rb.velocity.x > 0) {
            sr.flipX = false;
        }
    }

    public override void Jump() {
        if (isGrounded) { // Allows player to jump if grounded
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
            anim.Play("alienJump");
            AudioSource.PlayClipAtPoint(jumpSound, GetComponent<Transform>().position, 1.0f);
        }
        else if (currentAirJumps > 0) { // States that if they are in the air and have currentAirJumps left, they can jump in the air
            currentAirJumps -= 1;
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
            anim.Play("alienJump");
            AudioSource.PlayClipAtPoint(jumpSound, GetComponent<Transform>().position, 1.0f);
        }
    }

    private IEnumerator PowerUp() { // The Coroutine to run the powerup
        maxAirJumps = 4;
        yield return new WaitForSeconds(10f);
        maxAirJumps = 2;
    }

    void OnTriggerEnter2D(Collider2D powerUp) { // This will activate the powerup if one is picked up
        if (powerUp.gameObject.CompareTag("powerUp")) {
            Destroy(powerUp.gameObject);
            StopCoroutine(PowerUp());
            StartCoroutine(PowerUp());
        }
    }

}