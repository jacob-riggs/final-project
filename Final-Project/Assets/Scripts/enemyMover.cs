﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMover : MonoBehaviour {

    public Vector3 pos1;
    public Vector3 pos2;
    public float speed;

	// Update is called once per frame
	void Update () { //This will move the enemy between the two positions
        transform.position = Vector3.Lerp(pos1, pos2, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
	}

}
