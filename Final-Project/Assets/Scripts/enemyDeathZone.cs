﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyDeathZone : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D player) {
        player.transform.position = gameManager.spawn.CurrentSpawn;
        gameManager.spawn.currentLives--;
    }

}
