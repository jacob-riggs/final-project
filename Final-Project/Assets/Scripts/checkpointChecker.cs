﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpointChecker : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other) { // If the checkpoint is touched, it gets destroyed and sets the current spawn
        if (other.tag == "Player") {
            gameManager.spawn.CurrentSpawn = other.gameObject.transform.position;
            Destroy(this.gameObject);
        }
    }

}
