﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathZone : MonoBehaviour {

    // This function will check to see if anything leaves the play area
    public void OnTriggerExit2D(Collider2D player) {
        player.transform.position = gameManager.spawn.CurrentSpawn;
        gameManager.spawn.currentLives--;
    }
}
