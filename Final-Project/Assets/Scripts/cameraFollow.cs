﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

    //public GameObject player;
    public Transform player;

    public float levelLength;
    public float lowLevelLength;

    void Start() {
        if (!player){ // Finds the player in the beginning
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    void LateUpdate()
    {

        if (!player) { // Finds the new player after respawning
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        else if (player.transform.position.x > lowLevelLength && player.transform.position.x < levelLength) { // Follows the player
            transform.position = new Vector3(player.transform.position.x, 0f, -10f);
        }
    }

}